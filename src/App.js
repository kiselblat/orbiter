import React, { Suspense, useState, useRef} from 'react';
import { Canvas, useFrame, useLoader, useThree } from '@react-three/fiber';
import './App.css';
import { SVGRenderer } from 'three-stdlib';

// import { BufferGeometry, EllipseCurve, LineGeometry, TextureLoader } from 'three';
// import * as THREE from 'three';
// import SVGRenderer from 'three/examples/js/renderers/SVGRenderer.js';
// import Projector from 'three/examples/js/renderers/Projector.js';
import { Earth, Orbit, Scene, Torus } from './components';

function App() {

  // 
  // Earth State
  // 
  const [transparentEarth, setTransparentEarth] = useState(true);
  const [doubleSideEarth, setDoubleSideEarth] = useState(false);
  const [animatedEarth, setAnimatedEarth] = useState(true);

  // 
  // Orbit State:
  //
  const [xRadius, setXRadius] = useState(2);
  const [yRadius, setYRadius] = useState(2)

  //
  // Camera state: 
  // 
  const [fov, setFOV] = useState(75);
  const [cameraX, setCameraX] = useState(0);
  const [cameraY, setCameraY] = useState(0);
  const [cameraZ, setCameraZ] = useState(4);
  const [mouseControls, setMouseControls] = useState(true);

  // function BtnSVGExportClick() {
  //   console.log('export button click')
  //   // const { scene, camera } = useThree();
  //   var rendererSVG = new SVGRenderer();
    
  //   rendererSVG.setSize(window.innerWidth, window.innerHeight);
  //   rendererSVG.render(scene, camera);
  //   ExportToSVG(rendererSVG, "test.svg");
  // }
  
  // function ExportToSVG(rendererSVG, filename) {
  //   console.log('ExportToSVG called')
  //   var XMLS = new XMLSerializer();
  //   var svgfile = XMLS.serializeToString(rendererSVG.domElement);
  //   var svgData = svgfile;
  //   var preface = '<?xml version="1.0" standalone="no"?>\r\n';
  //   var svgBlob = new Blob([preface, svgData], {
  //     type: "image/svg+xml;charset=utf-8"
  //   });
  //   var svgUrl = URL.createObjectURL(svgBlob);
  //   var downloadLink = document.createElement("a");
    
  //   downloadLink.href = svgUrl;
  //   downloadLink.download = filename;
  //   document.body.appendChild(downloadLink);
  //   downloadLink.click();
  //   document.body.removeChild(downloadLink);
  // }

  return (
    <div className="App">

      <header className="App-header">
        Orbiter Sandbox
      </header>

      <div id={'scene'}>

        <Scene 
          // id="canvas-container" 
          mouseControls={mouseControls} 
          fov={fov}
          cameraX={cameraX} 
          cameraY={cameraY} 
          cameraZ={cameraZ} 
          // exportToSVG={ExportToSVG}
        >

          <Earth 
            animated={animatedEarth} 
            transparent={transparentEarth} 
            DoubleSide={doubleSideEarth} 
          />

          <Orbit 
            linePath={true}
            xRadius={xRadius} 
            yRadius={yRadius} 
          />

          {/* <Torus /> */}

        </Scene>

      </div>

      <div id={'controls'}>
        <form id="earth" name='earth'>
          <h4>Earth</h4>
          <fieldset>
            <legend>Surface</legend>
            <label htmlFor="transparentEarth" form='earth'>Earth transparency: </label>
            <input type='checkbox' id='transparentEarth' checked={transparentEarth} onChange={e => setTransparentEarth(e.target.checked)} /><br />
            <label htmlFor="doubleSideEarth" form='earth'>See back of texture map: </label>
            <input type='checkbox' id='doubleSideEarth' checked={doubleSideEarth} onChange={e => setDoubleSideEarth(e.target.checked)} /><br/>
          </fieldset>
          <fieldset>
            <legend>Animation</legend>
            <label htmlFor="animatedEarth" form='earth'>Earth rotation: </label>
            <input type='checkbox' id='animatedEarth' checked={animatedEarth} onChange={e => setAnimatedEarth(e.target.checked)} />
          </fieldset>
        </form>
        <form id="camera" name='camera'>
          <h4>Camera</h4>
          <fieldset>
            <legend>Camera position</legend>
            <label htmlFor="fov" form='camera'>FOV: </label>
            <input type="number" id="fov" value={fov} onChange={e => setFOV(e.target.value)} /><br />
            <label htmlFor="cameraX" form='camera'>CameraX: </label>
            <input type="number" id="cameraX" value={cameraX} onChange={e => setCameraX(e.target.value)} /><br />
            <label htmlFor="cameraY" form='camera'>CameraY: </label>
            <input type="number" id="cameraY" value={cameraY} onChange={e => setCameraY(e.target.value)} /><br />
            <label htmlFor="cameraZ" form='camera'>CameraZ: </label>
            <input type="number" id="cameraZ" value={cameraZ} onChange={e => setCameraZ(e.target.value)} /><br />
          </fieldset>
          <fieldset>
            <legend>Other</legend>
            <label htmlFor="mouseControls" form='camera'>Mouse Controls: </label>
            <input type='checkbox' id='mouseControls' checked={mouseControls} onChange={e => setMouseControls(e.target.checked)} />
            {/* <input id="btnSVGExport" type="button" value="Get as SVG" onClick={BtnSVGExportClick} /> */}
          </fieldset>
        </form>
      </div>

    </div>
  );
}

export default App;
