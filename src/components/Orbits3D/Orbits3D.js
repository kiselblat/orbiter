import React, { useState, Suspense } from 'react'

import { Canvas, useFrame, useLoader } from "@react-three/fiber";
import { OrbitControls, useTexture, Line } from "@react-three/drei";

import * as THREE from "three";

import './Orbits3D.scss'



function Lights() {
    return (
        <>
            <ambientLight />
            <pointLight position={[0, 0, 0]} />
        </>
    );
}

function Planet() {
    const textureProps = useTexture({
        // map: "/media/img/earth_texture.jpg"
        map: "/media/img/Nova_earth_map.png"
    });
    return (
        <mesh>
            <sphereGeometry args={[2.5, 32, 32]} />
            <meshPhysicalMaterial
                // color="#E1DC59"
                transparent={true}
                {...textureProps}
            // metalnessMap={metalness}
            // bumpMap={bump}
            // aoMap={ao}
            // normalMap={normal}
            // roughnessMap={rough}
            />
        </mesh>
    );
}

function rotatePoints(points, axis, angle) {
    var quat = new THREE.Quaternion();

    var axis;
    switch (axis) {// O.o it have to be a better way.
        case 'X':
            axis = new THREE.Vector3(1, 0, 0)
            break;
        case 'Y':
            axis = new THREE.Vector3(0, 1, 0)
            break;
        case 'Z':
            axis = new THREE.Vector3(0, 0, 1)
            break;
        default:
            axis = new THREE.Vector3(1, 0, 0)
            break;
    }

    quat.setFromAxisAngle(axis, angle * Math.PI / 180); // dont know why but it works T_T

    return points.map(p => {
        return p.applyQuaternion(quat);
    });
}


function Ecliptic({ offsetX, offsetY, xRadius = 1, zRadius = 1, rotateX, rotateY, rotateZ }) {
    var points = [];

    for (let index = 0; index < 360; index++) {
        var phi = (index / 360) * Math.PI * 2;
        var x = offsetX + xRadius * Math.cos(phi);
        var z = offsetY + zRadius * Math.sin(phi);
        points.push(new THREE.Vector3(x, 0, z));
    }

    // other Way to render the line
    // const lineGeometry = new THREE.BufferGeometry().setFromPoints(points);

    if (rotateX)
        points = rotatePoints(points, 'X', rotateX);
    if (rotateY)
        points = rotatePoints(points, 'Y', rotateY);
    if (rotateZ)
        points = rotatePoints(points, 'Z', rotateZ);


    points.push(points[0]); // close the ellipses to the first point to be the last one
    return (
        <>
            <Line
                points={points}       // Array of points
                color="#E18B00"                   // Default
                lineWidth={2.5}                   // In pixels (default)
            // {...lineProps}                  // All THREE.Line2 props are valid
            // {...materialProps}              // All THREE.LineMaterial props are valid
            />
            {/* The primary option to render a line, but the linewith does not work
            <line geometry={lineGeometry}>
                <lineBasicMaterial attach="material" color="#BFBBDA" linewidth={10} />
            </line> */}
        </>
    );
}


const Orbits3D = props => {
    const orbits = props.orbits || [];
    return (
        <Canvas
            style={{
                display: `block`,
                width: '100%',
                height: 675
            }} camera={{ position: [0, 20, 25], fov: 45 }}>
            <Suspense fallback={null}>


                <Planet />
                {orbits.map(o =>
                    <Ecliptic
                        {...o}
                    // xRadius={10}
                    // zRadius={10}
                    // offsetX={0}
                    // offsetY={0}
                    // rotateX={0}
                    // rotateY={0}
                    // rotateZ={0}
                    />
                )}

                <Lights />
                <OrbitControls />
            </Suspense>
        </Canvas>
    );
}



export default Orbits3D;
