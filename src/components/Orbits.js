import React, { useState, useEffect } from 'react';
import TabbedModal from './TabbedModal/TabbedModal';
import OrbitMaker from './OrbitMaker/OrbitMaker'
import OrbitSelector from './OrbitSelector/OrbitSelector';
import OrbitDisplay from './OrbitDisplay/OrbitDisplay';
import TeamPopUp from './TeamPopUp/TeamPopUp';
import Quiz from './Quiz/Quiz';
import ConceptualLesson from './ConceptualLesson/ConceptualLesson';
import StoryLesson from './StoryLesson/StoryLesson';

import Orbits3D from '../../partials/Orbits3D/Orbits3D';

const condition = true;


const OrbitsWrapper = (props) => {
    const [orbit1, setOrbit1] = useState({
        xRadius: 8,
        zRadius: 4,
        offsetX: 0,
        offsetY: 0,
        rotateX: 0,
        rotateY: 90,
        rotateZ: 0
    });

    const [orbit2, setOrbit2] = useState({
        xRadius: 10,
        zRadius: 5,
        offsetX: 0,
        offsetY: 0,
        rotateX: 45,
        rotateY: 0,
        rotateZ: 0
    });

    return (
        <>
            <h1>Prototipe, TODO: Apply diferent Textures!</h1>
            <input
                type="number"
                value={orbit1.xRadius}
                onChange={e => setOrbit1({ ...orbit1, xRadius: Number(e.target.value) })}
            />
            <input
                type="number"
                value={orbit1.zRadius}
                onChange={e => setOrbit1({ ...orbit1, zRadius: Number(e.target.value) })}
            />
            <input
                type="number"
                value={orbit1.offsetX}
                onChange={e => setOrbit1({ ...orbit1, offsetX: Number(e.target.value) })}
            />
            <input
                type="number"
                value={orbit1.offsetY}
                onChange={e => setOrbit1({ ...orbit1, offsetY: Number(e.target.value) })}
            />
            <input
                type="number"
                value={orbit1.rotateX}
                onChange={e => setOrbit1({ ...orbit1, rotateX: Number(e.target.value) })}
            />
            <input
                type="number"
                value={orbit1.rotateY}
                onChange={e => setOrbit1({ ...orbit1, rotateY: Number(e.target.value) })}
            />
            <input
                type="number"
                value={orbit1.rotateZ}
                onChange={e => setOrbit1({ ...orbit1, rotateZ: Number(e.target.value) })}
            />

            <Orbits3D orbits={[orbit1, orbit2]} />
            {
                // props.data.subtype === "concept" ?
                // <ConceptualLesson {...props}/>
                // :
                // <StoryLesson {...props}/>
            }

        </>
    );
}

export default OrbitsWrapper;