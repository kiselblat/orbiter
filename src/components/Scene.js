import React, { Suspense } from 'react';
import { Canvas, useThree } from '@react-three/fiber';
import SVGRenderer from 'three/examples/js/renderers/SVGRenderer.js';
import Projector from 'three/examples/js/renderers/Projector.js';
import { OrbitControls } from '@react-three/drei';
import { Dolly } from './';

export default function Scene(props) {

  
  // function SVGExportClick() {
  //   const { scene, camera } = useThree();
  //   var rendererSVG = new SVGRenderer();
    
  //   rendererSVG.setSize(window.innerWidth, window.innerHeight);
  //   rendererSVG.render(scene, camera);
  //   props.exportToSVG(rendererSVG, "test.svg");
  // }
  

  return (

    <Canvas 
    >
      <ambientLight intensity={0.1} />
      <directionalLight color='white' position={[2, 0, 5]} />

      <Suspense fallback={null}>

        {props.children}

        {props.mouseControls && <OrbitControls />}

      </Suspense>

      <Dolly fov={props.fov} cameraX={props.cameraX} cameraY={props.cameraY}  cameraZ={props.cameraZ} />

    </Canvas>

  )
}
