import { useEffect } from 'react';
import { useThree } from '@react-three/fiber';

export default function Dolly(props) {
  const { camera } = useThree()
  useEffect(() => {
    camera.fov = props.fov
    camera.position.x = props.cameraX;
    camera.position.y = props.cameraY;
    camera.position.z = props.cameraZ;
  }, [camera, props.fov, props.cameraX, props.cameraY, props.cameraZ])

  return null;
}
