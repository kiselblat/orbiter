import React, { useState, useRef} from 'react';
import { useFrame } from '@react-three/fiber';

export default function Satellite(props) {
  const satellite = useRef();
  const [t, setT] = useState(0)
  useFrame((state, delta) => {
    setT(t + 0.01);
    // satellite.current.rotation.x += 0.03;
    // satellite.current.rotation.y += 0.02;
    satellite.current.position.x = props.xRadius * Math.cos(t) + 0;
    satellite.current.position.y = props.yRadius * Math.sin(t) + 0;
  })

  return (
    <mesh ref={satellite} scale={0.01}>
      <cylinderGeometry />
      <meshStandardMaterial color={'lightblue'} />
    </mesh>
  )
}
