import React, { useRef } from 'react';
import { useFrame } from '@react-three/fiber';

export default function Torus(props) {
  const torusRef = useRef();

  useFrame(() => {
    // torusRef.current.rotation.x += 0.02;
    // torusRef.current.rotation.y += 0.02;
    torusRef.current.rotation.z += 0.02;
  });

  return (
    <mesh {... props}  rotation={[Math.PI / 2, 0, 0]} ref={torusRef}>
      <torusGeometry args={[2, 0.01, 8, 36, Math.PI * 3/2]} />
      <meshStandardMaterial color={"red"} />
    </mesh>
  );
};
