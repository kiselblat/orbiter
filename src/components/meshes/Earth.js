import React, { useRef} from 'react';
import { useFrame, useLoader } from '@react-three/fiber';
import { FrontSide, DoubleSide, TextureLoader } from 'three';

export default function Earth(props) {
  // This ref will give us direct access to this mesh
  const earth = useRef();
  // This takes an image and gives a texture map back
  // const earthMap = useLoader(TextureLoader, 'map.png');
  const earthMap = useLoader(TextureLoader, 'othermap.png');
  // We then use the ref to subscribe this mesh to the render-loop
  // and rotate is every frame
  useFrame((state, delta) => {if (props.animated) {earth.current.rotation.y += (props.rotationRate || 0.01)}})

  return (
    <mesh ref={earth} scale={1}>
      <sphereGeometry />
      {/* <meshStandardMaterial color={'lightblue'} map={earthMap} transparent /> */}
      {/* <meshStandardMaterial color={'lightblue'} map={earthMap} side={DoubleSide} transparent /> */}
      <meshStandardMaterial color={'lightblue'} map={earthMap} side={props.DoubleSide ? DoubleSide : FrontSide} transparent={props.transparent || false} />
    </mesh>
  )
}