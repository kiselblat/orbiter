import React from 'react';
import { Satellite, OrbitalPath } from '..';
import { OrbitalPathLine } from './OrbitalPath'

export default function Orbit(props) {

  return(
    <group  rotation={[Math.PI / 2, 0, 0]}>
      <Satellite 
        xRadius={props.xRadius}
        yRadius={props.yRadius}
      />
      {props.linePath ? 
        <OrbitalPathLine
          xRadius={props.xRadius}
          yRadius={props.yRadius}
        />
        :
        <OrbitalPath
          xRadius={props.xRadius}
          yRadius={props.yRadius}
        />
      }
    </group>
  )
}
