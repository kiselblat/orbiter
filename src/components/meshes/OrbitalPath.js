import React, { useCallback, useMemo, useRef, useState } from 'react';
// import { Canvas, useFrame, useLoader } from '@react-three/fiber';
import { EllipseCurve, TubeGeometry } from 'three';

export function OrbitalPathLine(props) {
  const orbit = useRef();

  const points = useMemo(() => new EllipseCurve(
    0, 0,                                                                       // ax, ay
    props.xRadius, props.yRadius,                                               // xRadius, yRadius
    0, 2 * Math.PI,                                                             // aStartAngle, aEndAngle
    false,                                                                      // aClockwise
    Math.PI * 2                                                                 // aRotation
  ).getPoints(100), [props.xRadius, props.yRadius])

  const onUpdate = useCallback(self => self.setFromPoints(points), [points])

  return (
    <line ref={orbit}>
      <bufferGeometry attach='geometry' onUpdate={onUpdate} />
      <lineBasicMaterial color={'yellow'} />
    </line>
  )
}

export default function OrbitalPath(props) {
  const orbit = useRef();

  // const ellipse = useMemo(() => new EllipseCurve(
  //   0, 0,                                                                       // ax, ay
  //   props.xRadius, props.yRadius,                                               // xRadius, yRadius
  //   0, 2 * Math.PI,                                                             // aStartAngle, aEndAngle
  //   false,                                                                      // aClockwise
  //   Math.PI * 2                                                                 // aRotation
  // ), [props.xRadius, props.yRadius])

  const ellipse = useMemo(() => new EllipseCurve(
    0, 0,                                                                       // ax, ay
    2, 2,                                               // xRadius, yRadius
    0, 2 * Math.PI,                                                             // aStartAngle, aEndAngle
    false,                                                                      // aClockwise
    Math.PI * 2                                                                 // aRotation
  ), [props.xRadius, props.yRadius])
  
  // const onUpdate = useCallback(self => self.setFromPoints(ellipse), [ellipse])

  return (
    <mesh ref={orbit}>
      {/* <tubeGeometry attach='geometry' onUpdate={onUpdate} /> */}
      <tubeBufferGeometry args={[ellipse, 64, 1, 2, false]} attach='geometry' />
      <meshStandardMaterial color={'yellow'} />
    </mesh>
  )
}

