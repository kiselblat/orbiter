import React from 'react';
import ReactDOM from 'react-dom';
import { render, events } from '@react-three/fiber'
import { SVGRenderer } from 'three-stdlib';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';


// // Create a custom renderer
// const gl = new SVGRenderer()
// // Append its dom-node
// const container = document.querySelector('#root')
// container.appendChild(gl.domElement)


// window.addEventListener('resize', () =>
//   render(<App />, container, {
//     // Inject renderer
//     gl,
//     // Use events, too ...
//     events,
//     camera: { position: [0, 0, 50] },
//     size: { width: window.innerWidth, height: window.innerHeight }
//   })
// )

// window.dispatchEvent(new Event('resize'))

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
